﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace appGraphique
{
    /// <summary>
    /// Logique d'interaction pour Window1.xaml
    /// </summary>
    public partial class EstearEgg : Window
    {

        
        
        public EstearEgg()
        {
            InitializeComponent();

        }

        private void ButSart_Click(object sender, RoutedEventArgs e)
        {
            
            if (Catherine.SelectedValue.ToString()   == "HAUT"    && Paulette.SelectedValue.ToString()   == "HAUT"
                && Louise.SelectedValue.ToString()   == "BAS"     && Philibert.SelectedValue.ToString()  == "BAS"
                && Herbert.SelectedValue.ToString()  == "GAUCHE"  && Josiane.SelectedValue.ToString()    == "DROITE"
                && Gertrude.SelectedValue.ToString() == "GAUCHE"  && Constentin.SelectedValue.ToString() == "DROITE"
                && Augustin.SelectedValue.ToString() == "B"       && Viviane.SelectedValue.ToString()    == "A")
            {
                Pokedex pok = new Pokedex();
                this.Close();
                pok.Show();
            }
            else
            {
                MessageBox.Show("Le code n'est pas correcte veuillez réessayer", "Mauvais Code(oui c'est le konami code)", MessageBoxButton.OK, MessageBoxImage.Exclamation);
               
            }

        }   
    }
}
