﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.ComponentModel;
using Model;


namespace appGraphique
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        
      


        public MainWindow()
        {
            InitializeComponent();

            ((App)Application.Current).InitCat();

            Affcons.DataContext = Manager.cat;
            

            butadd.Click += Butadd_Click;
            this.Closing += MainWindow_Closing;
                 
            
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                Manager.leDataManager.SaveCatalogue(Manager.file, Manager.cat);         
            }
            catch
            {
                MessageBoxResult MessageR = MessageBox.Show("Problème lors de la sauvagarde du fichier voulez vous quand même fermer l'application ?", "Erreur fichier", MessageBoxButton.YesNo, MessageBoxImage.Error);

                if (MessageR == MessageBoxResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

              

        private void Butadd_Click(object sender, RoutedEventArgs e)
        {
            Addcons addWin = new Addcons();
            addWin.ShowDialog();
            Consolle toto = addWin.GetItem();
            if (toto == null)
            {
              MessageBox.Show("Problème dans la saisie", "Erreur paramètre", MessageBoxButton.OK, MessageBoxImage.Error);
                            
            }
            else
            {
                Manager.cat.AddConsole(toto);
            }

            
        }
             

        private void Affcons_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Manager.cons = (Consolle)Affcons.SelectedItem;
        }

        private void Easter_egg_Click(object sender, RoutedEventArgs e)
        {
            EstearEgg ester = new EstearEgg();
            ester.Show();
        }
    }
}
