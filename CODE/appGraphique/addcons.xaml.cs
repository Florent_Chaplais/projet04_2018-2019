﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using Microsoft.Win32;

namespace appGraphique
{
    /// <summary>
    /// Logique d'interaction pour addcons.xaml
    /// </summary>
    public partial class Addcons : Window
    {
        String pathCons     = "/Srcimg;Component/srcimg/error_404_not_found.png";
        String pathConstruc = "/Srcimg;Component/srcimg/error_404_not_found.png";
        String pathJMvendu  = "/Srcimg;Component/srcimg/error_404_not_found.png";
        String pathJPvendu  = "/Srcimg;Component/srcimg/error_404_not_found.png";
        String pathPays     = "/Srcimg;Component/srcimg/error_404_not_found.png";

        public Addcons()
        {
            InitializeComponent();
        }

       private void ValidBut_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void Image_but_Cons (object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.gif, *.png) | *.jpg; *.jpeg; *.jpe; *.gif; *.png";
            dialog.InitialDirectory = @"C:\";
            dialog.Title = "Choisisez une image";

            if (dialog.ShowDialog() == true)
            {
                imgCons.Source = (ImageSource)new ImageSourceConverter().ConvertFromString(dialog.FileName);
                pathCons = dialog.FileName;
            }
        }
        private void Image_but_JMV(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.gif, *.png) | *.jpg; *.jpeg; *.jpe; *.gif; *.png";
            dialog.InitialDirectory = @"C:\";
            dialog.Title = "Choisisez une image";

            if (dialog.ShowDialog() == true)
            {
                imgJMV.Source = (ImageSource)new ImageSourceConverter().ConvertFromString(dialog.FileName);
                pathJMvendu = dialog.FileName;
            }
        }
        private void Image_but_JPV(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.gif, *.png) | *.jpg; *.jpeg; *.jpe; *.gif; *.png";
            dialog.InitialDirectory = @"C:\";
            dialog.Title = "Choisisez une image";

            if (dialog.ShowDialog() == true)
            {
                imgJPV.Source = (ImageSource)new ImageSourceConverter().ConvertFromString(dialog.FileName);
                pathJPvendu = dialog.FileName;
            }
        }
        private void Image_but_Construc(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.gif, *.png) | *.jpg; *.jpeg; *.jpe; *.gif; *.png";
            dialog.InitialDirectory = @"C:\";
            dialog.Title = "Choisisez une image";

            if (dialog.ShowDialog() == true)
            {
                imgConstruc.Source = (ImageSource)new ImageSourceConverter().ConvertFromString(dialog.FileName);
                pathConstruc = dialog.FileName;
            }
        }

        private void Image_but_Pays(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.gif, *.png) | *.jpg; *.jpeg; *.jpe; *.gif; *.png";
            dialog.InitialDirectory = @"C:\";
            dialog.Title = "Choisisez une image";

            if (dialog.ShowDialog() == true)
            {
                imgPays.Source = (ImageSource)new ImageSourceConverter().ConvertFromString(dialog.FileName);
                pathPays = dialog.FileName;
            }

        }
        public Consolle GetItem()
        {
            
            if (!(nbVente.Text.Trim().Equals("") 
                && nom.Text.Trim().Equals("") && constructeur.Text.Trim().Equals("") 
                && dateD.Text.Trim().Equals("") && JeuxPvendu.Text.Trim().Equals("") 
                && JeuxMvendu.Text.Trim().Equals("") && descritpion.Text.Trim().Equals("")
                && prixcons.Text.Trim().Equals("") && dateF.Text.Trim().Equals("")) 
                && (Regex.Match(prixcons.Text, "^[0-9]*$").Success)
                &&  Regex.Match(nbVente.Text, "^[0-9]*$").Success && Regex.Match(dateD.Text,"^[0-9]{4,4}$").Success
                &&  Regex.Match(dateF.Text, "^[0-9]{4,4}$").Success)
                
               
            {
                int notei = (int)note.Value;
                int nbventei = int.Parse(nbVente.Text);
                double prix = double.Parse(prixcons.Text);
                Consolle cons = new Consolle(nom.Text, constructeur.Text, dateD.Text, JeuxPvendu.Text, JeuxMvendu.Text,
                    descritpion.Text, notei, nbventei, prix, 
                    pathCons, pathJMvendu, pathJPvendu, pathConstruc, pays.Text, pathPays, innovation.Text, dateF.Text);
                return cons;
            }
            return null;
                   
        }
    }
}
