﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace appGraphique
{
    /// <summary>
    /// Logique d'interaction pour Window1.xaml
    /// </summary>
    public partial class Pokedex : Window
    {
        // MediaPlayer mediplay = new MediaPlayer();

       SoundPlayer sound;
        public Pokedex()
        {
            InitializeComponent();
            sound = new SoundPlayer("pokemon.wav");
            sound.Play();

            this.Closing += Pokedex_Closing;
            

        }

        private void Pokedex_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            sound.Stop();
        }
    }
}

