﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace appGraphique
{
    /// <summary>
    /// Logique d'interaction pour ModelBStatUserControl.xaml
    /// </summary>
    public partial class ModelBStatUserControl : UserControl
    {
        public ModelBStatUserControl()
        {
            InitializeComponent();
        }
    }
}
