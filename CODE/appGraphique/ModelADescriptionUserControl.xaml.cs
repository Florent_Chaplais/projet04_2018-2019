﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace appGraphique
{
    /// <summary>
    /// Logique d'interaction pour ModelADescriptionUserControl.xaml
    /// </summary>
    public partial class ModelADescriptionUserControl : UserControl
    {
        public ModelADescriptionUserControl()
        {
            InitializeComponent();
           

        }
        private void Eventsuppr(object sender, RoutedEventArgs e)
        {
            MessageBoxResult MessageR = MessageBox.Show($"Voulez-vous réellement supprimer la console {Manager.cons} ?", "Supprimer", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if(MessageR == MessageBoxResult.Yes){
                Manager.cat.DeletConsole(Manager.cons);
            }
        }

     
    }
}
