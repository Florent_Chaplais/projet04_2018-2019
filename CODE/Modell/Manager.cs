﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /**
     * <summary>Classe passerelle entre la vue et le model</summary> 
     */
    public class Manager
    {
        public static Catalogue cat;
        public static String file = "data.bin";
        public static IDataManager leDataManager;
        public static Consolle cons;

        public Manager()
        {
            leDataManager = new DataManagerSerialisation();
            cat = leDataManager.InitCatalogue(file);
        }



    }
}