﻿using System;
using System.Collections.ObjectModel;

namespace Model
{
    [Serializable]
    /**
     * <summary>Classe qui représente une console</summary>
     */
    public class Consolle
    {
        /**
         * <summary>Propriété du nom de la console</summary>
         */
        public string Nom { set; get; }
        /**
         * <summary>Propriété du constructeur de la console</summary>
         */
        public string Constructeur { set; get; }
        /**
         * <summary>Propriété de la date de début de production de la console</summary>
         */
        public string DateDebut { set; get; }
        /**
         * <summary>Propriété de la date de fin de production de la console</summary>
         */
        public string DateFin { set; get; }
        /**
         * <summary>Propriété du jeux le plus de la console</summary>
         */

        public string JeuxPlusVendu { set; get; }
        /**
         * <summary>Propriété du Jeux le moins vendu de la console</summary>
         */
        public string JeuxMoinsVendu { set; get; }
        /**
         * <summary>Propriété de la description de la console</summary>
         */
        public string Desc { set; get; }
        /**
         * <summary>Propriété de l'innavation apportée à la console</summary>
         */
        public string Innov { set; get; }
        /**
         * <summary>Propriété de la note de press de la console</summary>
         */
        public int Note { set; get; }
        /**
         * <summary>Propriété du nombre de vente de la console</summary>
         */
        public int NbVente { set; get; }
        /**
         * <summary>Propriété du prix actuel de la console</summary>
         */
        public double Prix { set; get; }
        /**
         * <summary>Propriété du chemin d'accès de l'image de la console</summary>
         */
        public string ImgCons { set; get; }
        /**
          * <summary>Propriété du chemin d'accès de l'image du constructeur de la console</summary>
          */
        public string ImgConstruct { set; get; }
        /**
          * <summary>Propriété du chemin d'accès de l'image du jeux le moins vendu de la console</summary>
          */
        public string ImgJeuMvendu { set; get; }
        /**
          * <summary>Propriété du chemin d'accès de l'image du jeux le plus vendu de la console la console</summary>
          */
        public string ImgJeuPvendu { set; get; }
        /**
          * <summary>Propriété du pays de la console</summary>
          */
        public string Pays { set; get; }
        /**
          * <summary>Propriété du chemin d'accès de l'image du pays de la console</summary>
          */
        public string ImgPays { set; get; }



        /**
         * <summary>Constructeur de la classe Consolle</summary>
         * 
         * <param name="nom">Désigne le nom de la console</param>
         * <param name="constructeur">Désigne le constructeur de la console</param>
         * <param name="dateDebut">Désigne la date de début de production de la console</param>
         * <param name="jeuxPlusVendu">Désigne le jeu vidéo le plus vendu sur la console</param>         
         * <param name="jeuxMoinsVendu">Désigne le jeu vidéo le moins vendu sur la console</param>
         * <param name="desc">Désigne la description de la console</param>
         * <param name="note">Désigne la note de press de la console</param>
         * <param name="nbVente">Désigne le nombre de vente de la console</param>
         * <param name="prix">Désigne le prix actuel de la console</param>
         * <param name="imgcons">Désigne le chemin d'accès de l'image pour la console</param>
         * <param name="imgjmvendu">Désigne le chemin d'accès de l'image pour le jeux le moins vendu de la console</param>
         * <param name="imgjpvendu">Désigne le chemin d'accès de l'image pour le jeux le plus vendu de la console</param>
         * <param name="imgconstruc">Désigne le chemin d'accès de l'image pour le constructeur de la console</param>
         * <param name="pays">Désigne le pays d'origine de la console</param>
         * <param name="imgpays">Désigne le chemin d'accès de l'image pour le pays de la console</param>
         * <param name="innov">Désigne l'innovation apportée à la console</param>
         * <param name="dateFin">Désigne la date de fin de production de la console</param>
         */
        public Consolle(string nom, string constructeur, string dateDebut,
                        string jeuxPlusVendu, string jeuxMoinsVendu,
                        string desc, int note, int nbVente,
                        double prix, string imgcons, string imgjmvendu,
                        string imgjpvendu, string imgconstruc, string pays,
                        string imgpays, string innov, string dateFin)
        {
            Nom = nom;
            Constructeur = constructeur;
            DateDebut = dateDebut;
            JeuxPlusVendu = jeuxPlusVendu;
            JeuxMoinsVendu = jeuxMoinsVendu;
            Desc = desc;
            Note = note;
            NbVente = nbVente;
            Prix = prix;
            ImgCons = imgcons;
            ImgConstruct = imgconstruc;
            ImgJeuPvendu = imgjpvendu;
            ImgJeuMvendu = imgjmvendu;
            Pays = pays;
            Innov = innov;
            ImgPays = imgpays;
            DateFin = dateFin;

        }

        /**
         * <summary>ToString de la classe Consolle</summary>
         * 
         * <returns>Retourne le nom de la Console</returns>
         */
        public override string ToString()
        {
            return $"{Nom}";
        }



    }
}