﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /**
     * <summary>Classe pour l'enregistrement des console dans un fichier</summary>
     */
    class DataManagerSerialisation : IDataManager
    {
        IFormatter formatter = new BinaryFormatter();


        /**
         * <summary>Permet d'initialiser le Catalogue avec les données sauvgardés dans un fichier</summary>
         * 
         * <param name="file">Désigne le nom du fichier voulue pour charger le catalogue</param>
         * 
         * <returns>Retourne le catalogue rempli</returns>
         */
        public Catalogue InitCatalogue(String file)
        {
            Catalogue cat;
            using (FileStream stream = File.OpenRead(file))
            {
                cat = formatter.Deserialize(stream) as Catalogue;
            }
            return cat;
        }

        /**
         * <summary>Permet de sauvgarder dans un fichier le catalogue</summary>
         * 
         * <param name="file">Désigne le nom du fichier voulue pour sauvgarder le catalogue</param>
         * <param name="cat">Désigne le catalogue à sauvgarder </param>
         */
        public void SaveCatalogue(String file, Catalogue cat)
        {
            using (FileStream stream = new FileStream(file, FileMode.Create))
            {
                formatter.Serialize(stream, cat);
            }

        }
    }
}