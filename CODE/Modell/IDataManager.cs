﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /**
     * <summary>Interface pour la gestion des données</summary>
     */
    public interface IDataManager
    {
        Catalogue InitCatalogue(String file);
        void SaveCatalogue(String file, Catalogue cat);
    }
}
