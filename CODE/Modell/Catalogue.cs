﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Model
{
    [Serializable]
    /**
     * <summary>Classe qui représnete une collection de console</summary>
     */
    public class Catalogue
    {
        /**
          * <summary>Propriété d'une liste de console</summary>
          */
        public ObservableCollection<Consolle> ListCons { set; get; }

        /**
         * <summary>Constructeur du catalogue</summary>
         */
        public Catalogue()
        {
            ListCons = new ObservableCollection<Consolle>();
        }

        /**
         * <summary>Ajoute une conole au Catalogue</summary>
         * 
         * <param name="cons">Objet de type Consolle à ajouter dans le Catalogue</param>
         */
        public void AddConsole(Consolle cons)
        {
            if (cons != null)
            {
                ListCons.Add(cons);
            }


        }

        /**
         * <summary>Supprime une console du Catalogue</summary>
         * 
         * <param name="cons">Objet de type Consolle a supprimer du Catalogue</param>
         */
        public void DeletConsole(Consolle cons)
        {
            ListCons.Remove(cons);
        }


    }
}