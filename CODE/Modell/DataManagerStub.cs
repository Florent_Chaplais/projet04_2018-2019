﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /**
     * <summary>Classe de donné brut pour le test du code</summary> 
     */
    class DataManagerStub : IDataManager
    {
        /**
         * <summary>Permet d'initiliser le catalogue avec les objet intégrer au Stub</summary>
         * 
         * <param name="file">N'est aps utilisé ici</param>
         */
        public Catalogue InitCatalogue(String file)
        {
            Catalogue cat = new Catalogue();

            cat.AddConsole(new Consolle("NES", "Nintendo", "1983", "Super Marios Bross", "Bart VS the world", "La Nintendo Entertainment System, couramment abrégée en NES, est une console de jeux vidéo de génération 8 bits " +
                "fabriquée par l'entreprise japonaise Nintendo et distribuée à partir de 1985 Son équivalent japonais est la Family Computer ou Famicom sortie quelques années avant, en 1983. " +
                "En Corée du Sud, la NES porta le nom de Comboy", 8, 61000000, 65, null, null, null, null, "Japon", null, "Première consolle a cartouche de Nintendo", "2003"));

            cat.AddConsole(new Consolle("MEGA Drive", "SEGA", "1988", "Sonic the Hedgehog", "Lakers versus Celtics and the NBA Playoffs", "La Mega Drive, ou Genesis en Amérique du Nord, est une console de jeux vidéo de quatrième génération conçue et commercialisée par le constructeur japonais Sega Enterprises, Ltd. " +
                "La Mega Drive est la troisième console de Sega, elle succède à la Master System. Sega sort d'abord la console au Japon sous le nom de Mega Drive en 1988, la console est ensuite lancée en 1989 en Amérique du Nord, renommée Genesis sur ce territoire. " +
                "En 1990, la console sort dans la plupart des autres territoires en tant que Mega Drive. ", 7, 39700000, 30, null, null, null, null, "Japon", null, "Dans sa première incursion dans le jeu en ligne, Sega crée Sega Meganet, qui débute au Japon le 3 novembre 1990. Opérant au travers d'une cartouche et d'un périphérique appelé Mega Modem, ce système permettait aux joueurs de la Mega Drive de jouer à dix-sept jeux en ligne.", "1999"));

            return cat;
        }

        /**
         * <summary>N'est pas utilisé</summary>
         */
        public void SaveCatalogue(String file, Catalogue cat)
        {

        }

    }
}