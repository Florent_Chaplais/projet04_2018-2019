﻿using System;

namespace ClassLibrary1
{
    [Serializable]
    public class Content
    {
        private String Nom { set; get; }


        public Content(string nom)
        {
            Nom = nom;
        }

        public override string ToString()
        {
            return $"{Nom}";
        }
    }
}
