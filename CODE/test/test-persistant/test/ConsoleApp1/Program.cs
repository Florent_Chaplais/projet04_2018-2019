﻿using ClassLibrary1;
using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {

        static Content cons;
        static Content cons2;
        static Content cons3;
        static Conttainer cat;
        static Conttainer cat2;
        static void Main(string[] args)
        {
            cons  = new Content("ccc");
            cons2 = new Content("zzz");
            cons3 = new Content("uuu");
            cat   = new Conttainer();
            cat2  = new Conttainer();
            IFormatter formatter = new BinaryFormatter();
            string file = "data.bin";

            cat.AddConsole(cons);
            cat.AddConsole(cons2);
            Console.WriteLine("========== contenue de la liste ==========");
            foreach (Content icons in cat.ListCont)
            {
                Console.WriteLine(icons);
            }

            //=========================================================================================================================
            Console.WriteLine("========== 1er serialisation ==========");
            using (FileStream stream = new FileStream(file, FileMode.Create))
            {
                formatter.Serialize(stream, cat);
            }


            using (FileStream stream = File.OpenRead(file))
            {
                cat = formatter.Deserialize(stream) as Conttainer;
            }

            foreach (Content icons in cat.ListCont)
            {
                Console.WriteLine(icons);
            }

            //=========================================================================================================================
            cat.AddConsole(cons3);

            Console.WriteLine("========== contenue de la liste ==========");
            foreach (Content icons in cat.ListCont)
            {
                Console.WriteLine(icons);
            }

            Console.WriteLine("========== 2eme serialisation ==========");

            using (FileStream stream = new FileStream(file, FileMode.Create))
            {
                formatter.Serialize(stream, cat);
            }

            using (FileStream stream = File.OpenRead(file))
            {
                cat = formatter.Deserialize(stream) as Conttainer;
            }

            foreach (Content icons in cat.ListCont)
            {
                Console.WriteLine(icons);
            }

            Console.Read();
            
               
           
            
        }
    }

}
